# Test de aptitud #

### Instrucciones ###

* Instalar Vagrant: http://docs.vagrantup.com/v2/installation/
* Iniciar la máquina virtual http://docs.vagrantup.com/v2/getting-started/index.html
* Abrir el navegador en http://localhost:8088
* Si el puerto está ocupado o no es posible abrirlo, modificar Vagrantfile y mapearlo a otro puerto

### Notas ###

Las tareas a realizar fueron pensadas para cubrir una amplia gama de habilidades; la no realización de una tarea no excluye a las demas.

Para entregar el código se requiere que se hagan los commits necesarios en el código y se devuelva un tar|zip|rar o similar con el repositorio listo para hacer el push.